Entity Bundle Scaffold
======================

> Provides Drush commands for creating node, taxonomy, paragraph & eck types and for generating entity bundle classes & controllers.

## Why?
- Managing entity types & bundles by clicking through the
  interface is inefficient, time consuming and not developer friendly.
- Creating entity bundle classes & controllers is equally repetitive and time
  consuming

## Installation

This package requires PHP 7.3, Drupal 9.3 or higher. The Drush commands
require version 11 or higher. The package can be installed using
Composer:

```bash
 composer require drupal/entity_bundle_scaffold
```

## How does it work?
### Commands
This package provides a whole range of Drush commands for managing
entity types & bundles and for generating code.

For more information about command aliases, arguments, options & usage
examples, call the command with the `-h` / `--help` argument

#### Drupal core
- `entity:bundle-class-generate`: Generate an entity bundle class
- `nodetype:create`: Create a new node type
- `vocabulary:create`: Create a new vocabulary

#### [eck](https://www.drupal.org/project/eck) module
- `eck:bundle:create`: Create a new eck entity type
- `eck:bundle:delete`: Delete an eck entity type
- `eck:type:create`: Delete an eck entity type

#### [paragraphs](https://www.drupal.org/project/paragraphs) module
- `paragraphs:type:create`: Create a new paragraph type

#### [wmcontroller](https://github.com/wieni/wmcontroller) module
- `wmcontroller:generate`: Generate a wmcontroller controller

### Code generator
This package provides Drupal services & Drush commands/hooks that can be
used to generate entity bundle classes and controllers for the wmcontroller module.

Controllers are generated with a single _show_ method, having the entity
injected as an argument and rendering a template following our naming
conventions. The template itself is not (yet) generated.

Entity bundle classes are generated with field getters. The content of the
getters is based on the field type and can be customized through
`EntityBundleClassMethodGenerator` plugins. Out of the box, implementations for all
common field types are provided.

## Changelog
All notable changes to this project will be documented in the
[CHANGELOG](CHANGELOG.md) file.

## Security
If you discover any security-related issues, please email
[security@wieni.be](mailto:security@wieni.be) instead of using the issue
tracker.
