<?php

/**
 * @file
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the information provided in the annotation.
 *
 * @param array $generators
 *   The array of generator plugins, keyed by the machine-readable name.
 *
 * @see \Drupal\entity_bundle_scaffold\Annotation\EntityBundleClassMethodGenerator
 */
function hook_entity_bundle_class_method_generator_alter(array &$generators) {
  $generators['link']['class'] = \Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator\FieldHelperLink::class;
}

/**
 * @} End of "addtogroup hooks".
 */
