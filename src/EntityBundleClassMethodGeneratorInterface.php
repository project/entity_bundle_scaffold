<?php

namespace Drupal\entity_bundle_scaffold;

use Drupal\Core\Field\FieldDefinitionInterface;
use PhpParser\Builder\Method;

/**
 * Interface for entity bundle class method generator plugins.
 */
interface EntityBundleClassMethodGeneratorInterface {

  /**
   * Build a getter method implementation for a field.
   */
  public function buildGetter(FieldDefinitionInterface $field, Method $method, array &$uses): void;

  /**
   * Build a setter method implementation for a field.
   */
  public function buildSetter();

}
