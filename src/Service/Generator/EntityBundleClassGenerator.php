<?php

namespace Drupal\entity_bundle_scaffold\Service\Generator;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorManager;
use Drupal\entity_bundle_scaffold\Service\Helper\IdentifierNaming;
use Drupal\entity_bundle_scaffold\Service\Helper\PhpParser;
use Drupal\entity_bundle_scaffold\Service\Helper\StringCapitalisation;
use PhpParser\BuilderFactory;
use PhpParser\Comment;
use PhpParser\Node\Stmt;
use PhpParser\ParserFactory;

/**
 * A service for generating entity bundle classes.
 */
class EntityBundleClassGenerator {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * The settings config of this module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * A factory for PHP parser builders.
   *
   * @var \PhpParser\BuilderFactory
   */
  protected $builderFactory;

  /**
   * A factory for PHP parsers.
   *
   * @var \PhpParser\ParserFactory
   */
  protected $parserFactory;

  /**
   * The plugin manager for entity bundle classes, if applicable.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   *
   * @see \Drupal\entity_model\ModelPluginManager
   * @see \Drupal\wmmodel\ModelPluginManager
   */
  protected $pluginManager;

  /**
   * The plugin manager for entity bundle class method generators.
   *
   * @var \Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorManager
   */
  protected $methodGeneratorManager;

  /**
   * A helper service for parsing PHP.
   *
   * @var \Drupal\entity_bundle_scaffold\Service\Helper\PhpParser
   */
  protected $phpParserHelper;

  /**
   * Constructs an EntityBundleClassGenerator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The filesystem service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \PhpParser\BuilderFactory $builderFactory
   *   A factory for PHP parser builders.
   * @param \PhpParser\ParserFactory $parserFactory
   *   A factory for PHP parsers.
   * @param \Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorManager $methodGeneratorManager
   *   The plugin manager for entity bundle class method generators.
   * @param \Drupal\entity_bundle_scaffold\Service\Helper\PhpParser $phpParserHelper
   *   A helper service for parsing PHP.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    EntityFieldManagerInterface $entityFieldManager,
    FileSystemInterface $fileSystem,
    ExtensionPathResolver $extensionPathResolver,
    ConfigFactoryInterface $configFactory,
    BuilderFactory $builderFactory,
    ParserFactory $parserFactory,
    EntityBundleClassMethodGeneratorManager $methodGeneratorManager,
    PhpParser $phpParserHelper
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityFieldManager = $entityFieldManager;
    $this->fileSystem = $fileSystem;
    $this->extensionPathResolver = $extensionPathResolver;
    $this->config = $configFactory->get('entity_bundle_scaffold.settings');
    $this->builderFactory = $builderFactory;
    $this->parserFactory = $parserFactory;
    $this->methodGeneratorManager = $methodGeneratorManager;
    $this->phpParserHelper = $phpParserHelper;
  }

  /**
   * Set the plugin manager if it exists.
   *
   * @see entity_bundle_scaffold.services.yml
   */
  public function setPluginManager(PluginManagerInterface $pluginManager): void {
    $this->pluginManager = $pluginManager;
  }

  /**
   * Clear all entity bundle class-related caches.
   */
  public function clearEntityBundleCaches(): void {
    // Clear cached bundle classes.
    $this->entityTypeBundleInfo->clearCachedBundles();

    // Make sure the plugin cache is up-to-date.
    if (isset($this->pluginManager)) {
      $this->pluginManager->clearCachedDefinitions();
    }
  }

  /**
   * Generate a new entity bundle class.
   */
  public function generateNew(string $entityType, string $bundle, string $module): Stmt\Namespace_ {

    $className = $this->buildClassName($entityType, $bundle, $module, TRUE);
    $namespaceName = $this->buildNamespaceName($entityType, $module);

    $baseClass = new \ReflectionClass($this->getBaseClass($entityType));
    $namespace = $this->builderFactory->namespace($namespaceName);
    $class = $this->builderFactory->class($className);

    $use = $this->builderFactory->use($baseClass->getName());
    if ($className === $baseClass->getShortName()) {
      $alias = $className . 'Base';
      $namespace->addStmt($use->as($alias));
      $class->extend($alias);
    }
    else {
      if ($baseClass->getNamespaceName() !== $namespaceName) {
        $namespace->addStmt($use);
      }

      $class->extend($baseClass->getShortName());
    }

    foreach ($this->getCustomFields($entityType, $bundle) as $field) {
      if (!$result = $this->buildFieldGetter($field, $baseClass->getName())) {
        continue;
      }

      [$method, $uses] = $result;

      $class->addStmt($method);
      $namespace->addStmts($uses);
    }

    $classNode = $class->getNode();
    if ($this->areModelsAnnotationBased()) {
      $docComment = new Comment\Doc(sprintf(<<<EOT
            /**
             * @Model(
             *     entity_type = "%s",
             *     bundle = "%s",
             * )
             */
            EOT
            , $entityType, $bundle));
      $classNode->setDocComment($docComment);
    }

    $namespace->addStmt($classNode);

    $namespaceNode = $namespace->getNode();
    $this->phpParserHelper->cleanUseStatements($namespaceNode);

    return $namespaceNode;
  }

  /**
   * Update an existing entity bundle class with getters for all current fields.
   */
  public function generateExisting(string $entityType, string $bundle): ?Stmt\Namespace_ {
    return $this->appendFieldGettersToExistingClass($entityType, $bundle, $this->getCustomFields($entityType, $bundle));
  }

  /**
   * Update an existing entity bundle class with getters for some fields.
   */
  public function appendFieldGettersToExistingClass(string $entityType, string $bundle, array $fields): ?Stmt\Namespace_ {
    $this->clearEntityBundleCaches();

    $definition = $this->entityTypeManager->getDefinition($entityType);
    $className = $this->entityTypeManager->getStorage($entityType)->getEntityClass($bundle);

    // Only edit custom classes.
    if ($className === $definition->getOriginalClass()) {
      return NULL;
    }

    // Must have an existing class.
    try {
      $class = new \ReflectionClass($className);
    }
    catch (\ReflectionException $e) {
      return NULL;
    }

    // Parse the existing file & extract the namespace node.
    $parser = $this->parserFactory->create(ParserFactory::PREFER_PHP7);
    $input = $parser->parse(file_get_contents($class->getFileName()));

    if (empty($input)) {
      return NULL;
    }

    /** @var \PhpParser\Node\Stmt\Namespace_ $namespace */
    $namespace = $input[0];

    // Add the new method to the class.
    foreach ($namespace->stmts as $i => $statement) {
      if (!$statement instanceof Stmt\Class_) {
        continue;
      }

      foreach ($fields as $field) {
        if (!$result = $this->buildFieldGetter($field, $className)) {
          continue;
        }

        [$method, $uses] = $result;

        // Check for existing methods with the same body
        // and don't create a new method if any are found.
        $existingMethods = array_filter(
          $statement->getMethods(),
          function (Stmt\ClassMethod $existingStmt) use ($method): bool {
            $existingStmt = clone $existingStmt;
            $existingStmt->name = $method->getNode()->name;
            return $this->phpParserHelper->compareNodes($existingStmt, $method->getNode());
          }
        );

        if (!empty($existingMethods)) {
          continue;
        }

        $getterMethodName = $this->buildFieldGetterName($field, $className);
        $methodNode = $method->getNode();
        $methodNode->name = $getterMethodName;

        // Add statements.
        foreach ($uses as $use) {
          if ($use instanceof Builder\Use_) {
            $namespace->stmts[] = $use->getNode();
          }
          elseif ($use instanceof Stmt\Use_) {
            $namespace->stmts[] = $use;
          }
        }

        $namespace->stmts[$i]->stmts[] = $methodNode;
      }
    }

    $this->phpParserHelper->cleanUseStatements($namespace);

    return $namespace;
  }

  /**
   * Get the path of an existing entity bundle class.
   */
  public function getEntityBundleClassPath(string $entityType, string $bundle): ?string {
    $definition = $this->entityTypeManager->getDefinition($entityType);
    $className = $this->entityTypeManager->getStorage($entityType)->getEntityClass($bundle);

    // Only edit custom classes.
    if ($className === $definition->getOriginalClass()) {
      return NULL;
    }

    // Must have an existing class.
    try {
      $class = new \ReflectionClass($className);
    }
    catch (\ReflectionException $e) {
      return NULL;
    }

    return $class->getFileName();
  }

  /**
   * Build the path to an entity bundle class.
   */
  public function buildEntityBundleClassPath(string $entityType, string $bundle, string $module): string {
    $className = $this->buildClassName($entityType, $bundle, $module);
    $parts = array_slice(explode('\\', $className), 2);

    return sprintf(
      '%s/src/%s.php',
      $this->fileSystem->realpath(
        $this->extensionPathResolver->getPath('module', $module)
      ),
      implode('/', $parts)
    );
  }

  /**
   * Check for a module for annotation-based entity bundle classes.
   *
   * @see setPluginManager()
   */
  public function areModelsAnnotationBased(): bool {
    return isset($this->pluginManager);
  }

  /**
   * Build the namespace for an entity bundle class.
   */
  protected function buildNamespaceName(string $entityType, string $module): string {
    $namespacePattern = $this->config->get('generators.bundle_class.namespace_pattern')
      ?? 'Drupal\{module}\Entity\{entityType}';

    $entityType = StringCapitalisation::toPascalCase($entityType);
    $entityType = IdentifierNaming::stripInvalidCharacters($entityType);

    return str_replace(
      ['{module}', '{entityType}'],
      [$module, $entityType],
      $namespacePattern
    );
  }

  /**
   * Build the class name for an entity bundle class.
   */
  protected function buildClassName(string $entityType, string $bundle, string $module, bool $shortName = FALSE): string {
    $label = StringCapitalisation::toPascalCase($bundle);
    $label = IdentifierNaming::stripInvalidCharacters($label);

    if ($label === '') {
      $label = StringCapitalisation::toPascalCase($entityType) . StringCapitalisation::toPascalCase($bundle);
    }

    if (IdentifierNaming::isReservedKeyword($label)) {
      $label .= 'Bundle';
    }

    if ($shortName) {
      return StringCapitalisation::toPascalCase($label);
    }

    return sprintf('%s\%s', $this->buildNamespaceName($entityType, $module), $label);
  }

  /**
   * Build the method name for a field getter.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field
   *   The definition of the field.
   * @param string|null $className
   *   The name of the class the method will be added to.
   *   This is used to check whether a method with this name already exists.
   */
  protected function buildFieldGetterName(FieldDefinitionInterface $field, ?string $className): string {
    $source = $this->config->get('generators.bundle_class.field_getter_name_source') ?? 'label';

    if ($source === 'name') {
      if ($field->getName() === sprintf('field_%s', $field->getTargetBundle())) {
        $fieldPrefixes = ['field_'];
      }
      else {
        $fieldPrefixes = [
          sprintf('field_%s', $field->getTargetBundle()),
          'field_',
        ];
      }
      $label = str_replace($fieldPrefixes, '', $field->getName());
    }
    else {
      $label = $field->getLabel();
    }

    $label = StringCapitalisation::toPascalCase($label);
    $label = IdentifierNaming::stripInvalidCharacters($label);

    $getterMethodName = 'get' . $label;

    // Check for existing methods with the same name
    // and rename the new method if any are found.
    if ($className) {
      try {
        $class = new \ReflectionClass($className);
      }
      catch (\ReflectionException $e) {
        return $getterMethodName;
      }

      $index = 1;
      $original = $getterMethodName;

      while ($class->hasMethod($getterMethodName)) {
        $getterMethodName = $original . $index;
        $index++;
      }
    }

    return $getterMethodName;
  }

  /**
   * Build the PHP code for a field getter.
   */
  protected function buildFieldGetter($field, string $className): ?array {
    if (!$field instanceof FieldDefinitionInterface) {
      return NULL;
    }

    $fieldsToIgnore = $this->config->get('generators.bundle_class.fields_to_ignore') ?? [];

    if (in_array($field->getName(), $fieldsToIgnore, TRUE)) {
      return NULL;
    }

    $id = $field->getType();
    if (!$this->methodGeneratorManager->hasDefinition($id)) {
      $id = '_field_item';
    }

    $getterMethodName = $this->buildFieldGetterName($field, $className);
    $method = $this->builderFactory->method($getterMethodName)->makePublic();
    $uses = [];

    /** @var \Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorInterface $generator */
    $generator = $this->methodGeneratorManager->createInstance($id);
    $generator->buildGetter($field, $method, $uses);

    return [$method, $uses];
  }

  /**
   * Get the base class the entity bundle class will extend.
   */
  protected function getBaseClass(string $entityTypeId): ?string {
    $baseClasses = $this->config->get('generators.bundle_class.base_classes') ?? [];

    if (isset($baseClasses[$entityTypeId])) {
      return $baseClasses[$entityTypeId];
    }

    foreach ($this->pluginManager->getDefinitions() as $entityTypeDefinition) {
      // Only entity type classes.
      if (isset($entityTypeDefinition['bundle'])) {
        continue;
      }

      if (!isset($entityTypeDefinition['entity_type'])) {
        continue;
      }

      if ($entityTypeDefinition['entity_type'] !== $entityTypeId) {
        continue;
      }

      return $entityTypeDefinition['class'];
    }

    $definition = $this->entityTypeManager->getDefinition($entityTypeId, FALSE);

    if ($definition) {
      return $definition->getOriginalClass();
    }

    return NULL;
  }

  /**
   * Get all non-base field definitions on an entity type and bundle.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions.
   */
  protected function getCustomFields(string $entityType, string $bundle): array {
    return array_filter(
      $this->entityFieldManager->getFieldDefinitions($entityType, $bundle),
      static function (FieldDefinitionInterface $field): bool {
        return $field->getFieldStorageDefinition()->getProvider() === 'field';
      }
    );
  }

}
