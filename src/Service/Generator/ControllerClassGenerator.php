<?php

namespace Drupal\entity_bundle_scaffold\Service\Generator;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileSystemInterface;
use Drupal\entity_bundle_scaffold\Service\Helper\PhpParser;
use Drupal\wmcontroller\ViewBuilder\ViewBuilder;
use Drupal\entity_bundle_scaffold\Service\Helper\IdentifierNaming;
use Drupal\entity_bundle_scaffold\Service\Helper\StringCapitalisation;
use PhpParser\BuilderFactory;
use PhpParser\Comment;
use PhpParser\Node\Stmt;

/**
 * A service for generating entity bundle controller classes.
 */
class ControllerClassGenerator {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * The settings config of this module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * A factory for PHP parser builders.
   *
   * @var \PhpParser\BuilderFactory
   */
  protected $builderFactory;

  /**
   * A helper service for parsing PHP.
   *
   * @var \Drupal\entity_bundle_scaffold\Service\Helper\PhpParser
   */
  protected $phpParserHelper;

  /**
   * Constructs a ControllerClassGenerator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The filesystem service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \PhpParser\BuilderFactory $builderFactory
   *   A factory for PHP parser builders.
   * @param \Drupal\entity_bundle_scaffold\Service\Helper\PhpParser $phpParserHelper
   *   A helper service for parsing PHP.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    ExtensionPathResolver $extensionPathResolver,
    ConfigFactoryInterface $configFactory,
    BuilderFactory $builderFactory,
    PhpParser $phpParserHelper
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->extensionPathResolver = $extensionPathResolver;
    $this->config = $configFactory->get('entity_bundle_scaffold.settings');
    $this->builderFactory = $builderFactory;
    $this->phpParserHelper = $phpParserHelper;
  }

  /**
   * Generate a new entity bundle controller class.
   */
  public function generateNew(string $entityType, string $bundle, string $module): Stmt\Namespace_ {
    $className = $this->buildClassName($entityType, $bundle, $module, TRUE);
    $namespaceName = $this->buildNamespaceName($entityType, $module);
    $entityClass = new \ReflectionClass(
      $this->entityTypeManager->getStorage($entityType)->getEntityClass($bundle)
    );

    $variableName = StringCapitalisation::toCamelCase($entityClass->getShortName());
    $templatePath = sprintf(
      '%s.%s',
      StringCapitalisation::toKebabCase($entityType),
      str_replace('_', '-', $bundle)
    );

    $namespace = $this->builderFactory->namespace($namespaceName);
    $class = $this->builderFactory->class($className);

    $namespace->addStmt($this->builderFactory->use($entityClass->getName()));

    if ($baseClass = $this->config->get('generators.controller.base_class')) {
      $baseClass = new \ReflectionClass($baseClass);
      $namespace->addStmt($this->builderFactory->use($baseClass->getName()));
      $class->extend($baseClass->getShortName());
    }

    // Add method to class.
    $method = $this->builderFactory->method('show');
    $method->makePublic()
      ->addParam($this->builderFactory->param($variableName)
        ->setType($entityClass->getShortName()));
    $method->addStmt(
      $this->phpParserHelper->parseExpression(sprintf('return $this->view(\'%s\', [\'%s\' => $%s]);', $templatePath, $variableName, $variableName))
    );
    $namespace->addStmt($this->builderFactory->use(ViewBuilder::class));
    $method->setReturnType('ViewBuilder');
    $class->addStmt($method);

    // Add annotation to class.
    $classNode = $class->getNode();
    $docComment = new Comment\Doc(sprintf(
        <<<EOT
          /**
           * @Controller(
           *     entity_type = "%s",
           *     bundle = "%s",
           * )
           */
          EOT,
        $entityType,
        $bundle
    ));
    $classNode->setDocComment($docComment);

    // Add class to namespace.
    $namespace->addStmt($classNode);

    $node = $namespace->getNode();
    $this->phpParserHelper->cleanUseStatements($node);

    return $node;
  }

  /**
   * Build the path to an entity bundle controller class.
   */
  public function buildControllerPath(string $entityType, string $bundle, string $module): string {
    $className = $this->buildClassName($entityType, $bundle, $module);
    $parts = array_slice(explode('\\', $className), 2);

    return sprintf(
      '%s/src/%s.php',
      $this->fileSystem->realpath(
        $this->extensionPathResolver->getPath('module', $module)
      ),
      implode('/', $parts)
    );
  }

  /**
   * Build the namespace for an entity bundle controller class.
   */
  public function buildNamespaceName(string $entityType, string $module): string {
    $namespacePattern = $this->config->get('generators.controller.namespace_pattern')
       ?? 'Drupal\{module}\Controller\{entityType}';

    $entityType = StringCapitalisation::toPascalCase($entityType);
    $entityType = IdentifierNaming::stripInvalidCharacters($entityType);

    return str_replace(
      ['{module}', '{entityType}'],
      [$module, $entityType],
      $namespacePattern
    );
  }

  /**
   * Build the class name for an entity bundle class.
   */
  public function buildClassName(string $entityType, string $bundle, string $module, bool $shortName = FALSE): string {
    $label = StringCapitalisation::toPascalCase($bundle);
    $label = IdentifierNaming::stripInvalidCharacters($label);
    $label .= 'Controller';

    if ($shortName) {
      return $label;
    }

    return sprintf('%s\%s', $this->buildNamespaceName($entityType, $module), $label);
  }

}
