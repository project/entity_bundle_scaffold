<?php

namespace Drupal\entity_bundle_scaffold\Service\Helper;

use Composer\Semver\Comparator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldTypePluginManager;
use PhpParser\BuilderFactory;
use PhpParser\ParserFactory;

/**
 * Helper methods for generating entity bundle classes.
 */
class EntityBundleClassMethodGeneratorHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManager
   */
  protected $fieldTypePluginManager;

  /**
   * A factory for PHP parser builders.
   *
   * @var \PhpParser\BuilderFactory
   */
  protected $builderFactory;

  /**
   * A factory for PHP parsers.
   *
   * @var \PhpParser\ParserFactory
   */
  protected $parserFactory;

  /**
   * The settings config of this module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs an EntityBundleClassMethodGeneratorHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Field\FieldTypePluginManager $fieldTypePluginManager
   *   The field type plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \PhpParser\BuilderFactory $builderFactory
   *   A factory for PHP parser builders.
   * @param \PhpParser\ParserFactory $parserFactory
   *   A factory for PHP parsers.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    FieldTypePluginManager $fieldTypePluginManager,
    ConfigFactoryInterface $configFactory,
    BuilderFactory $builderFactory,
    ParserFactory $parserFactory
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fieldTypePluginManager = $fieldTypePluginManager;
    $this->builderFactory = $builderFactory;
    $this->parserFactory = $parserFactory;
    $this->config = $configFactory->get('entity_bundle_scaffold.settings');
  }

  /**
   * Determine whether a field's cardinality is multiple.
   */
  public function isFieldMultiple(FieldDefinitionInterface $field): bool {
    return $field->getFieldStorageDefinition()->getCardinality() !== 1;
  }

  /**
   * Returns the full classname of the entity of a field.
   */
  public function getFieldEntityClass(FieldDefinitionInterface $field): string {
    $targetType = $field->getFieldStorageDefinition()->getSetting('target_type');
    $definition = $this->entityTypeManager->getDefinition($targetType);
    $handlerSettings = $field->getSetting('handler_settings');

    if (empty($handlerSettings['target_bundles'])) {
      return $definition->getClass();
    }

    return $this->entityTypeManager
      ->getStorage($definition->id())
      ->getEntityClass(reset($handlerSettings['target_bundles']));
  }

  /**
   * Returns the full classname of a field type.
   */
  public function getFieldTypeClass(FieldDefinitionInterface $field): ?string {
    $definition = $this->fieldTypePluginManager->getDefinition($field->getType());

    return $definition['class'] ?? NULL;
  }

  /**
   * Convert a string representation of a PHP statement into a PhpParser node.
   */
  public function parseExpression(string $expression): array {
    $parser = $this->parserFactory->create(ParserFactory::PREFER_PHP7);
    $statements = $parser->parse('<?php ' . $expression . ';');

    return $statements;
  }

  /**
   * Check whether the configured PHP version supports nullable types.
   */
  public function supportsNullableTypes(): bool {
    return Comparator::greaterThanOrEqualTo($this->config->get('php_version'), 7.1);
  }

  /**
   * Check whether the configured PHP version supports arrow functions.
   */
  public function supportsArrowFunctions(): bool {
    return Comparator::greaterThanOrEqualTo($this->config->get('php_version'), 7.4);
  }

  /**
   * Check whether the configured PHP version supports optional chaining.
   */
  public function supportsOptionalChaining(): bool {
    return Comparator::greaterThanOrEqualTo($this->config->get('php_version'), 8.0);
  }

}
