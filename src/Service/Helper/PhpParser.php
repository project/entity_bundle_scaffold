<?php

namespace Drupal\entity_bundle_scaffold\Service\Helper;

use Drupal\entity_bundle_scaffold\PhpParser\NodeVisitor\ClassMethodNormalizer;
use PhpParser\Node;
use PhpParser\Node\Stmt;
use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;

/**
 * A helper service for parsing PHP.
 */
class PhpParser {

  /**
   * A factory for PHP parsers.
   *
   * @var \PhpParser\ParserFactory
   */
  protected $parserFactory;

  /**
   * Constructs a PhpParser object.
   *
   * @param \PhpParser\ParserFactory $parserFactory
   *   A factory for PHP parsers.
   */
  public function __construct(
    ParserFactory $parserFactory
  ) {
    $this->parserFactory = $parserFactory;
  }

  /**
   * Parse a PHP expression into a PhpParser statement object.
   */
  public function parseExpression(string $expression): Stmt {
    $parser = $this->parserFactory->create(ParserFactory::PREFER_PHP7);
    $statements = $parser->parse('<?php ' . $expression . ';');

    return $statements[0];
  }

  /**
   * Deduplicate and sort use statements.
   */
  public function cleanUseStatements(Stmt\Namespace_ $namespace): Stmt\Namespace_ {
    $uses = [];

    // Deduplicate.
    foreach ($namespace->stmts as $i => $statement) {
      if (!$statement instanceof Stmt\Use_) {
        continue;
      }

      foreach ($statement->uses as $j => $use) {
        $name = (string) $use->name;
        if (in_array($name, $uses, TRUE)) {
          unset($statement->uses[$j]);
          continue;
        }

        $uses[] = $name;
      }

      if (empty($statement->uses)) {
        unset($namespace->stmts[$i]);
      }
    }

    // Sort.
    usort(
      $namespace->stmts,
      function (Stmt $a, Stmt $b): int {
        if ($a instanceof Stmt\Class_ && $b instanceof Stmt\Use_) {
          return 1;
        }
        return -1;
      }
    );

    return $namespace;
  }

  /**
   * Checks whether two PHP parser nodes are functionally the same.
   */
  public function compareNodes(): bool {
    if (func_num_args() < 2) {
      return TRUE;
    }

    $toString = function (Node $node) {
      // Remove attributes.
      $traverser = new NodeTraverser();
      $traverser->addVisitor(new ClassMethodNormalizer());

      $nodes = $traverser->traverse([$node]);
      $node = $nodes[0];

      // Convert to array.
      $node = $node->jsonSerialize();

      // Sort keys.
      $this->ksortRecursive($node);

      // Convert to string.
      $node = json_encode($node);

      return $node;
    };

    // Prevent side effects.
    $args = array_map(
      function ($arg) {
        return clone $arg;
      },
      func_get_args()
    );

    $first = array_shift($args);

    foreach ($args as $arg) {
      $results = array_map($toString, [$first, $arg]);

      if ($results[0] === $results[1]) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Sort an array by key recursively.
   */
  protected function ksortRecursive(array &$array): bool {
    foreach ($array as &$value) {
      if (is_array($value)) {
        $this->ksortRecursive($value);
      }
    }

    return ksort($array);
  }

}
