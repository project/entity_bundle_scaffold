<?php

namespace Drupal\entity_bundle_scaffold\Service\Helper;

/**
 * Helper methods for converting the capitalisation of strings.
 */
class StringCapitalisation {

  /**
   * Convert a string to camelCase.
   */
  public static function toCamelCase(string $input): string {
    return lcfirst(self::toPascalCase($input));
  }

  /**
   * Convert a string to PascalCase.
   */
  public static function toPascalCase(string $input): string {
    $input = preg_replace('/\-|\s/', '_', $input);

    return str_replace('_', '', ucwords($input, '_'));
  }

  /**
   * Convert a string to kebab-case.
   */
  public static function toKebabCase(string $input): string {
    $input = preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '-$0', $input);

    return strtolower(ltrim($input, '-'));
  }

}
