<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for list_float fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "list_float",
 *   provider = "options",
 * )
 */
class ListFloat extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'float';
  }

}
