<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for telephone fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "telephone",
 *   provider = "telephone",
 * )
 */
class Telephone extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'string';
  }

}
