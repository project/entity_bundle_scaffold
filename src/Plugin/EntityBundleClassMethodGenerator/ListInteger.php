<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for list_integer fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "list_integer",
 *   provider = "options",
 * )
 */
class ListInteger extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'int';
  }

}
