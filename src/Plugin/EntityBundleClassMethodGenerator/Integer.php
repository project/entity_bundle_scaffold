<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for integer fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "integer",
 *   provider = "core",
 * )
 */
class Integer extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'int';
  }

}
