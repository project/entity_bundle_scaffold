<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorBase;
use PhpParser\Builder\Method;
use PhpParser\Node\NullableType;

/**
 * A getter method generator base class for returning scalar values.
 */
abstract class BaseScalarType extends EntityBundleClassMethodGeneratorBase {

  /**
   * {@inheritdoc}
   */
  public function buildGetter(FieldDefinitionInterface $field, Method $method, array &$uses): void {
    $scalarType = static::getType();

    if ($this->helper->isFieldMultiple($field)) {
      $expression = sprintf(
        'return array_column(
                $this->get(\'%s\')->getValue(),
                \'value\'
            )',
        $field->getName()
      );
    }
    elseif ($this->shouldCastToType()) {
      $expression = sprintf('return (%s) $this->get(\'%s\')->value;', static::getType(), $field->getName());
    }
    else {
      $expression = sprintf('return $this->get(\'%s\')->value;', $field->getName());
    }

    if ($this->helper->isFieldMultiple($field)) {
      $method->setReturnType('array');
      $method->setDocComment(sprintf('/** @return %s[] */', $scalarType));
    }
    elseif ($field->isRequired()) {
      $method->setReturnType($scalarType);
    }
    elseif ($this->helper->supportsNullableTypes()) {
      $method->setReturnType(new NullableType($scalarType));
    }
    else {
      $method->setDocComment(sprintf('/** @return %s|null */', $scalarType));
    }

    $method->addStmts($this->helper->parseExpression($expression));
  }

  /**
   * Get the type keyword that will be used type hinting.
   */
  abstract public static function getType(): string;

  /**
   * Whether the value should be cast to its scalar type.
   */
  protected function shouldCastToType(): bool {
    return FALSE;
  }

}
