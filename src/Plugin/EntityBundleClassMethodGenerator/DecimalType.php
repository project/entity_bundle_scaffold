<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for decimal fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "decimal",
 *   provider = "core",
 * )
 */
class DecimalType extends FloatType {
}
