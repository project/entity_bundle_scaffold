<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for string fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "string",
 *   provider = "core",
 * )
 */
class StringType extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'string';
  }

}
