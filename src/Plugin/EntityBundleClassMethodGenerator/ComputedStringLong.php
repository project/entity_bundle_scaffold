<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for computed_string_long fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "computed_string_long",
 *   provider = "computed_field",
 * )
 */
class ComputedStringLong extends StringLong {
}
