<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorBase;
use PhpParser\Builder\Method;
use PhpParser\Node\NullableType;

/**
 * A getter method generator base class for returning the field item instances.
 */
abstract class BaseFieldItem extends EntityBundleClassMethodGeneratorBase {

  /**
   * {@inheritdoc}
   */
  public function buildGetter(FieldDefinitionInterface $field, Method $method, array &$uses): void {
    $fieldTypeClass = $this->helper->getFieldTypeClass($field) ?? self::getType();
    $fieldTypeClass = new \ReflectionClass($fieldTypeClass);
    $uses[] = $this->builderFactory->use($fieldTypeClass->getName());

    $expression = $this->helper->isFieldMultiple($field)
            ? sprintf('return iterator_to_array($this->get(\'%s\'));', $field->getName())
            : sprintf('return $this->get(\'%s\')->first();', $field->getName());

    if ($this->helper->isFieldMultiple($field)) {
      $method->setReturnType('array');
      $method->setDocComment(sprintf('/** @return %s[] */', $fieldTypeClass->getShortName()));
    }
    elseif ($field->isRequired()) {
      $method->setReturnType($fieldTypeClass->getShortName());
    }
    elseif ($this->helper->supportsNullableTypes()) {
      $method->setReturnType(new NullableType($fieldTypeClass->getShortName()));
    }
    else {
      $method->setDocComment(sprintf('/** @return %s|null */', $fieldTypeClass->getShortName()));
    }

    $method->addStmts($this->helper->parseExpression($expression));
  }

  /**
   * {@inheritdoc}
   */
  public static function getType(): ?string {
    return NULL;
  }

}
