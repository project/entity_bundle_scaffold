<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for text_with_summary fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "text_with_summary",
 *   provider = "core",
 * )
 */
class TextWithSummary extends Text {
}
