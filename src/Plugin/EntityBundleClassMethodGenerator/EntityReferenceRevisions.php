<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for entity_reference_revisions fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "entity_reference_revisions",
 *   provider = "entity_reference_revisions",
 * )
 */
class EntityReferenceRevisions extends EntityReference {
}
