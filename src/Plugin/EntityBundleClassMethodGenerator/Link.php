<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

use Drupal\link\LinkItemInterface;

/**
 * A getter method generator for link fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "link",
 *   provider = "link",
 * )
 */
class Link extends BaseFieldItem {

  /**
   * {@inheritdoc}
   */
  public static function getType(): ?string {
    return LinkItemInterface::class;
  }

}
