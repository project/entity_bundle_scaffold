<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for file fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "file",
 *   provider = "file",
 * )
 */
class File extends EntityReference {
}
