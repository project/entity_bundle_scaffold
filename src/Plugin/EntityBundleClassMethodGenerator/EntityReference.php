<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorBase;
use Drupal\entity_model\Field\TranslatableEntityReferenceFieldItemList;
use PhpParser\Builder\Method;
use PhpParser\Node\NullableType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A getter method generator for entity_reference fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "entity_reference",
 *   provider = "core",
 * )
 */
class EntityReference extends EntityBundleClassMethodGeneratorBase {

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->fieldTypeManager = $container->get('plugin.manager.field.field_type');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildGetter(FieldDefinitionInterface $field, Method $method, array &$uses): void {
    $fieldEntityClass = $this->helper->getFieldEntityClass($field);
    $fieldEntityClass = new \ReflectionClass($fieldEntityClass);
    $shouldGetTranslations = $this->shouldGetTranslations($field);

    $uses[] = $this->builderFactory->use($fieldEntityClass->getName());

    $expression = $this->helper->isFieldMultiple($field)
      ? sprintf('return $this->get(\'%s\')->%s;', $field->getName(), $shouldGetTranslations ? 'translations' : 'referencedEntities()')
      : sprintf('return $this->get(\'%s\')->%s;', $field->getName(), $shouldGetTranslations ? 'translation' : 'entity');

    if ($this->helper->isFieldMultiple($field)) {
      $method->setReturnType('array');
      $method->setDocComment(sprintf('/** @return %s[] */', $fieldEntityClass->getShortName()));
    }
    elseif ($this->helper->supportsNullableTypes()) {
      $method->setReturnType(new NullableType($fieldEntityClass->getShortName()));
    }
    else {
      $method->setDocComment(sprintf('/** @return %s|null */', $fieldEntityClass->getShortName()));
    }

    $method->addStmts($this->helper->parseExpression($expression));
  }

  /**
   * Determines whether the Entity Model translation getters can be used.
   *
   * @see https://www.drupal.org/project/entity_model/issues/3309363
   */
  protected function shouldGetTranslations(FieldDefinitionInterface $field): string {
    $fieldTypeDefinition = $this->fieldTypeManager->getDefinition('entity_reference');
    if ($fieldTypeDefinition['list_class'] !== TranslatableEntityReferenceFieldItemList::class) {
      return FALSE;
    }

    $targetType = $field->getFieldStorageDefinition()->getSetting('target_type');
    if (empty($targetType)) {
      return FALSE;
    }

    $targetBundles = $field->getSetting('handler_settings')['target_bundles'] ?? [];
    if ($targetBundles === []) {
      return FALSE;
    }

    $bundleInfo = $this->entityTypeBundleInfo->getBundleInfo($targetType);
    foreach ($targetBundles as $targetBundle) {
      if (!empty($bundleInfo[$targetBundle]['translatable'])) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
