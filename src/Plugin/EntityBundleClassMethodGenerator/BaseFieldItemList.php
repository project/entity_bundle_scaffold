<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorBase;
use PhpParser\Builder\Method;
use PhpParser\Node\NullableType;

/**
 * A getter method generator base class for returning field item list instances.
 */
abstract class BaseFieldItemList extends EntityBundleClassMethodGeneratorBase {

  /**
   * {@inheritdoc}
   */
  public function buildGetter(FieldDefinitionInterface $field, Method $method, array &$uses): void {
    $fieldTypeClass = $this->helper->getFieldTypeClass($field) ?? self::getType();
    $fieldTypeClass = new \ReflectionClass($fieldTypeClass);
    $uses[] = $this->builderFactory->use($fieldTypeClass->getName());

    $expression = sprintf('return $this->get(\'%s\');', $field->getName());

    if ($field->isRequired()) {
      $method->setReturnType($fieldTypeClass->getShortName());
    }
    elseif ($this->helper->supportsNullableTypes()) {
      $method->setReturnType(new NullableType($fieldTypeClass->getShortName()));
    }
    else {
      $method->setDocComment(sprintf('/** @return %s|null */', $fieldTypeClass->getShortName()));
    }

    $method->addStmts($this->helper->parseExpression($expression));
  }

  /**
   * The class name of the field item list.
   *
   * Can be overridden in case you want to return an interface or
   * alternative implementation instead of the default class.
   */
  public static function getType(): ?string {
    return NULL;
  }

}
