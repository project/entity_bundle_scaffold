<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A fallback getter method generator for returning the field item instances.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "_field_item",
 *   provider = "core",
 * )
 *
 * @see \Drupal\Core\Field\FieldItemInterface
 */
class FieldItem extends BaseFieldItem {
}
