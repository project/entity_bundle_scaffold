<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

use Drupal\Core\Field\FieldDefinitionInterface;
use PhpParser\Builder\Method;

/**
 * A getter method generator for boolean fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "boolean",
 *   provider = "core",
 * )
 */
class Boolean extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'bool';
  }

  /**
   * {@inheritdoc}
   */
  public function buildGetter(FieldDefinitionInterface $field, Method $method, array &$uses): void {
    parent::buildGetter($field, $method, $uses);
    $method->setReturnType(self::getType());
  }

  /**
   * {@inheritdoc}
   */
  protected function shouldCastToType(): bool {
    return TRUE;
  }

}
