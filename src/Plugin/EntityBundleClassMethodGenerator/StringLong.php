<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for string_long fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "string_long",
 *   provider = "core",
 * )
 */
class StringLong extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'string';
  }

}
