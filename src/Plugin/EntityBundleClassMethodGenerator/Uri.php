<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for uri fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "uri",
 *   provider = "core",
 * )
 */
class Uri extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'string';
  }

}
