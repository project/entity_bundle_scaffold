<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for float fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "float",
 *   provider = "core",
 * )
 */
class FloatType extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'float';
  }

}
