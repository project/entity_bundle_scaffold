<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for office_hours fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "office_hours",
 *   provider = "office_hours",
 * )
 */
class OfficeHours extends BaseFieldItemList {

  /**
   * {@inheritdoc}
   */
  public static function getType(): ?string {
    return 'Drupal\office_hours\Plugin\Field\FieldType\OfficeHoursItemListInterface';
  }

}
