<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for timestamp fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "timestamp",
 *   provider = "core",
 * )
 */
class Timestamp extends StringType {
}
