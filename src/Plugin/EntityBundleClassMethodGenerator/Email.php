<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for email fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "email",
 *   provider = "core",
 * )
 */
class Email extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'string';
  }

}
