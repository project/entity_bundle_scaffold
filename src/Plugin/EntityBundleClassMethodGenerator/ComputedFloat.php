<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for computed_float fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "computed_float",
 *   provider = "computed_field",
 * )
 */
class ComputedFloat extends FloatType {
}
