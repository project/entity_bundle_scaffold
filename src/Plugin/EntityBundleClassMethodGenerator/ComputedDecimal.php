<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for computed_decimal fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "computed_decimal",
 *   provider = "computed_field",
 * )
 */
class ComputedDecimal extends DecimalType {
}
