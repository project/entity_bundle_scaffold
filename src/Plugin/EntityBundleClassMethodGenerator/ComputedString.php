<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for computed_string fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "computed_string",
 *   provider = "computed_field",
 * )
 */
class ComputedString extends StringType {
}
