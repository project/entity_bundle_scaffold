<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for text_long fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "text_long",
 *   provider = "core",
 * )
 */
class TextLong extends Text {
}
