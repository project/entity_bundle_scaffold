<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for list_string fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "list_string",
 *   provider = "options",
 * )
 */
class ListString extends BaseScalarType {

  /**
   * {@inheritdoc}
   */
  public static function getType(): string {
    return 'string';
  }

}
