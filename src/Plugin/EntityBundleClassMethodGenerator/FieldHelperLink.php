<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorBase;
use PhpParser\Builder\Method;

/**
 * An alternative getter method generator using the formatLink(s) field helper.
 */
class FieldHelperLink extends EntityBundleClassMethodGeneratorBase {

  /**
   * {@inheritdoc}
   */
  public function buildGetter(FieldDefinitionInterface $field, Method $method, array &$uses): void {
    $methodName = $this->helper->isFieldMultiple($field) ? 'formatLinks' : 'formatLink';
    $expression = sprintf('return $this->%s(\'%s\');', $methodName, $field->getName());

    $method->setReturnType('array');
    $method->addStmts($this->helper->parseExpression($expression));
  }

}
