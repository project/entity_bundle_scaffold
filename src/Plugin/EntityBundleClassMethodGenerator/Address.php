<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

use Drupal\address\AddressInterface;

/**
 * A getter method generator for address fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "address",
 *   provider = "address",
 * )
 */
class Address extends BaseFieldItem {

  /**
   * {@inheritdoc}
   */
  public static function getType(): ?string {
    return AddressInterface::class;
  }

}
