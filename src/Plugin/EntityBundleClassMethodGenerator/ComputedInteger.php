<?php

namespace Drupal\entity_bundle_scaffold\Plugin\EntityBundleClassMethodGenerator;

/**
 * A getter method generator for computed_integer fields.
 *
 * @EntityBundleClassMethodGenerator(
 *   id = "computed_integer",
 *   provider = "computed_field",
 * )
 */
class ComputedInteger extends Integer {
}
