<?php

namespace Drupal\entity_bundle_scaffold\PhpParser\NodeVisitor;

use Node\Identifier;
use Stmt\ClassMethod;
use PhpParser\Node;
use PhpParser\NodeVisitorAbstract;

/**
 * Normalizes class method nodes.
 */
class ClassMethodNormalizer extends NodeVisitorAbstract {

  /**
   * {@inheritdoc}
   */
  public function leaveNode(Node $node): void {
    $node->setAttributes([]);

    if ($node instanceof ClassMethod) {
      $node->flags = [];

      if (!$node->name instanceof Identifier) {
        $node->name = new Identifier($node->name);
      }
    }
  }

}
