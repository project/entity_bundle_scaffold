<?php

namespace Drupal\entity_bundle_scaffold\Hooks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_bundle_scaffold\Service\Generator\ControllerClassGenerator;
use Drupal\entity_bundle_scaffold\Service\Generator\EntityBundleClassGenerator;
use Drupal\field\Entity\FieldConfig;
use PhpParser\PrettyPrinterAbstract;
use Symfony\Component\Filesystem\Filesystem;

class EntityInsertHooks {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The filesystem.
   *
   * @var \Symfony\Component\Filesystem\Filesystem
   */
  protected $fileSystem;

  /**
   * The entity bundle class generator.
   *
   * @var \Drupal\entity_bundle_scaffold\Service\Generator\EntityBundleClassGenerator
   */
  protected $bundleClassgenerator;

  /**
   * The controller class generator.
   *
   * @var \Drupal\entity_bundle_scaffold\Service\Generator\ControllerClassGenerator
   */
  protected $controllerClassGenerator;

  /**
   * The pretty printer.
   *
   * @var PrettyPrinterAbstract
   */
  protected $prettyPrinter;

  /**
   * EntityInsertHooks constructor.
   *
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param MessengerInterface $messenger
   *   The messenger.
   * @param EntityBundleClassGenerator $bundleClassGenerator
   *   The entity bundle class generator.
   * @param ControllerClassGenerator $controllerClassGenerator
   *   The controller class generator.
   * @param PrettyPrinterAbstract $prettyPrinter
   *   The pretty printer.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory,
    MessengerInterface $messenger,
    EntityBundleClassGenerator $bundleClassGenerator,
    ControllerClassGenerator $controllerClassGenerator,
    PrettyPrinterAbstract $prettyPrinter
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $configFactory;
    $this->messenger = $messenger;
    $this->fileSystem = new Filesystem();
    $this->bundleClassgenerator = $bundleClassGenerator;
    $this->controllerClassGenerator = $controllerClassGenerator;
    $this->prettyPrinter = $prettyPrinter;
  }

  public function onFieldConfigInsert(FieldConfig $entity): void {
    $this->appendFieldGetter($entity);
  }

  public function onBundleInsert(ConfigEntityInterface $entity): void {
    $this->generateBundleClass($entity);
    $this->generateController($entity);
  }

  protected function appendFieldGetter(FieldConfig $entity): void {
    $entityTypeId = $entity->getTargetEntityTypeId();
    $bundle = $entity->getTargetBundle();

    $settings = $this->configFactory->get('entity_bundle_scaffold.settings');
    $module = $settings->get('generators.bundle_class.output_module');

    if ($module === NULL) {
      return;
    }

    if (!$statement = $this->bundleClassgenerator->appendFieldGettersToExistingClass($entityTypeId, $bundle, [$entity])) {
      return;
    }

    $output = $this->prettyPrinter->prettyPrintFile([$statement]);
    $destination = $this->bundleClassgenerator->getEntityBundleClassPath($entityTypeId, $bundle)
      ?? $this->bundleClassgenerator->buildEntityBundleClassPath($entityTypeId, $bundle, $module);

    if (!file_exists($destination)) {
      return;
    }

    $this->fileSystem->remove($destination);
    $this->fileSystem->appendToFile($destination, $output);

    $bundleClassName = $this->entityTypeManager
      ->getStorage($entityTypeId)
      ->getEntityClass($bundle);

    $this->messenger->addStatus($this->t('Added new getter for @fieldName to @bundleClassName', [
      '@fieldName' => $entity->getName(),
      '@bundleClassName' => $bundleClassName,
    ]));
  }

  protected function generateBundleClass(ConfigEntityInterface $entity): void {
    $entityType = $entity->getEntityType()->getBundleOf();
    $bundle = $entity->id();

    if ($entityType === NULL) {
      return;
    }

    $settings = $this->configFactory->get('entity_bundle_scaffold.settings');
    $module = $settings->get('generators.bundle_class.output_module');

    if ($module === NULL) {
      return;
    }

    $statements = [];
    $definition = $this->entityTypeManager
      ->getDefinition($entityType);
    $existingClassName = $this->entityTypeManager
      ->getStorage($entityType)
      ->getEntityClass($bundle);
    $hasExisting = FALSE;

    if ($existingClassName && $existingClassName !== $definition->getClass()) {
      $hasExisting = TRUE;
      $destination = (new \ReflectionClass($existingClassName))->getFileName();

      if (file_exists($destination)) {
        return;
      }

      $statements[] = $this->bundleClassgenerator->generateExisting($entityType, $bundle);
    }
    else {
      $destination = $this->bundleClassgenerator->buildEntityBundleClassPath($entityType, $bundle, $module);
      $statements[] = $this->bundleClassgenerator->generateNew($entityType, $bundle, $module);
    }

    $output = $this->prettyPrinter->prettyPrintFile($statements);
    $this->fileSystem->remove($destination);
    $this->fileSystem->appendToFile($destination, $output);

    $this->messenger->addStatus($this->t('Successfully @operation entity bundle class.', [
      '@operation' => $hasExisting ? $this->t('updated') : $this->t('created'),
    ]));

    if (!$this->bundleClassgenerator->areModelsAnnotationBased()) {
      $this->messenger->addStatus($this->t('Don\'t forget to register your bundle class through a <a href="@hookEntityBundleInfoAlterUrl">hook_entity_bundle_info_alter implementation</a>, or by using the <a href="@entityModelUrl">Entity Model module<a/>.', [
        '@hookEntityBundleInfoAlterUrl' => 'https://www.drupal.org/node/3191609',
        '@entityModelUrl' => 'https://www.drupal.org/project/entity_model',
      ]));
    }

    // Clear cached bundle classes.
    $this->bundleClassgenerator->clearEntityBundleCaches();
  }

  protected function generateController(ConfigEntityInterface $entity): void {
    $entityType = $entity->getEntityType()->getBundleOf();
    $bundle = $entity->id();

    if ($entityType === NULL) {
      return;
    }

    $settings = $this->configFactory->get('entity_bundle_scaffold.settings');
    $module = $settings->get('generators.controller.output_module');

    if ($module === NULL) {
      return;
    }

    $className = $this->controllerClassGenerator->buildClassName($entityType, $bundle, $module);
    $destination = $this->controllerClassGenerator->buildControllerPath($entityType, $bundle, $module);
    $statements = [];

    try {
      new \ReflectionClass($className);

      if (file_exists($destination)) {
        return;
      }
    }
    catch (\ReflectionException $e) {
      // Noop.
    }

    $statements[] = $this->controllerClassGenerator->generateNew($entityType, $bundle, $module);
    $output = $this->prettyPrinter->prettyPrintFile($statements);
    $this->fileSystem->remove($destination);
    $this->fileSystem->appendToFile($destination, $output);

    $this->messenger->addStatus($this->t('Successfully created controller class.'));
  }

}
