<?php

namespace Drupal\entity_bundle_scaffold\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an entity bundle class method generator annotation object.
 *
 * @see \Drupal\entity_bundle_scaffold\EntityBundleClassMethodGeneratorManager
 * @see plugin_api
 *
 * @Annotation
 */
class EntityBundleClassMethodGenerator extends Plugin {
}
