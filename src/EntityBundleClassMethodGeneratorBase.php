<?php

namespace Drupal\entity_bundle_scaffold;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use PhpParser\Builder\Method;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for entity bundle class method generator plugins.
 */
abstract class EntityBundleClassMethodGeneratorBase extends PluginBase implements EntityBundleClassMethodGeneratorInterface, ContainerFactoryPluginInterface {

  /**
   * A factory for PHP parser builders.
   *
   * @var \PhpParser\BuilderFactory
   */
  protected $builderFactory;

  /**
   * The entity bundle class method generator helper service.
   *
   * @var \Drupal\entity_bundle_scaffold\Service\Helper\EntityBundleClassMethodGeneratorHelper
   */
  protected $helper;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id, $plugin_definition
  ) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->builderFactory = $container->get('entity_bundle_scaffold.php_parser.builder_factory');
    $instance->helper = $container->get('entity_bundle_scaffold.entity_bundle_class_method_generator.helper');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function buildGetter(FieldDefinitionInterface $field, Method $method, array &$uses): void;

  /**
   * {@inheritdoc}
   */
  public function buildSetter(): void {
    // @todo Implement field setters
  }

}
