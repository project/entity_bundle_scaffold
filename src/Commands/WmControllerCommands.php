<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_bundle_scaffold\Service\Generator\ControllerClassGenerator;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\field\EntityTypeBundleAskTrait;
use Drush\Drupal\Commands\field\EntityTypeBundleValidationTrait;
use PhpParser\PrettyPrinter\Standard as PrettyPrinter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Drush commands for generating entity bundle controllers.
 *
 * Works with the wmcontroller module.
 */
class WmControllerCommands extends DrushCommands {

  use EntityTypeBundleAskTrait;
  use EntityTypeBundleValidationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The controller class generator.
   *
   * @var \Drupal\entity_bundle_scaffold\Service\Generator\ControllerClassGenerator
   */
  protected $controllerClassGenerator;

  /**
   * The PHP code pretty printer.
   *
   * @var \PhpParser\PrettyPrinter\Standard
   */
  protected $prettyPrinter;

  /**
   * The filesystem service.
   *
   * @var \Symfony\Component\Filesystem\Filesystem
   */
  protected $fileSystem;

  /**
   * Constructs a WmControllerCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\entity_bundle_scaffold\Service\Generator\ControllerClassGenerator $controllerClassGenerator
   *   The controller class generator.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    ConfigFactoryInterface $configFactory,
    ControllerClassGenerator $controllerClassGenerator
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->configFactory = $configFactory;
    $this->controllerClassGenerator = $controllerClassGenerator;
    $this->prettyPrinter = new PrettyPrinter();
    $this->fileSystem = new Filesystem();
  }

  /**
   * Generate a wmcontroller controller.
   *
   * @param string|null $entityType
   *   The machine name of the entity type.
   * @param string|null $bundle
   *   The machine name of the bundle.
   * @param array $options
   *   The command options.
   *
   * @command wmcontroller:generate
   * @aliases wmcontroller-generate,wmcg
   *
   * @option output-module
   *   The module in which to generate the file
   *
   * @option show-machine-names
   *   Show machine names instead of labels in option lists.
   *
   * @usage drush wmcontroller-generate taxonomy_term tag
   *   Generate a controller.
   * @usage drush wmcontroller:generate
   *   Generate a controller and fill in the remaining
   *   information through prompts.
   *
   * @validate-module-enabled wmcontroller
   */
  public function generateController(?string $entityType = NULL, ?string $bundle = NULL, array $options = [
    'output-module' => InputOption::VALUE_REQUIRED,
    'show-machine-names' => InputOption::VALUE_OPTIONAL,
  ]): void {
    if (empty($options['output-module'])) {
      throw new \InvalidArgumentException('You must specify an output module through --output-module or through configuration.');
    }

    $this->input->setArgument('entityType', $entityType = $entityType ?? $this->askEntityType());
    $this->validateEntityType($entityType);

    $this->input->setArgument('bundle', $bundle = $bundle ?? $this->askBundle());
    $this->validateBundle($entityType, $bundle);

    $className = $this->controllerClassGenerator->buildClassName($entityType, $bundle, $options['output-module']);
    $destination = $this->controllerClassGenerator->buildControllerPath($entityType, $bundle, $options['output-module']);
    $statements = [];

    try {
      new \ReflectionClass($className);

      if (file_exists($destination) && !$this->io()->confirm(sprintf('%s already exists. Replace existing class?', $className), FALSE)) {
        return;
      }
    }
    catch (\ReflectionException $e) {
      // Noop.
    }

    $statements[] = $this->controllerClassGenerator->generateNew($entityType, $bundle, $options['output-module']);
    $output = $this->prettyPrinter->prettyPrintFile($statements);
    $this->fileSystem->remove($destination);
    $this->fileSystem->appendToFile($destination, $output);

    $this->logger()->success('Successfully created controller class.');
  }

  /**
   * Add a default value for the output-module option, if necessary.
   *
   * @hook init wmcontroller:generate
   */
  public function init(): void {
    $module = $this->input->getOption('output-module');

    if (!$module) {
      $default = $this->configFactory
        ->get('entity_bundle_scaffold.settings')
        ->get('generators.controller.output_module');

      $this->input->setOption('output-module', $default);
    }
  }

}
