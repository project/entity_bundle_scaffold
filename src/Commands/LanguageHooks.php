<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Consolidation\AnnotatedCommand\AnnotationData;
use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\language\Entity\ContentLanguageSettings;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Drush hooks for integrating the language module with other commands.
 */
class LanguageHooks extends DrushCommands {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a LanguageHooks object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    LanguageManagerInterface $languageManager
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->languageManager = $languageManager;
  }

  /**
   * Add language related options to the command.
   *
   * @hook option nodetype:create
   */
  public function hookNodeTypeOptions(Command $command, AnnotationData $annotationData): void {
    $this->doAddOptions($command);
  }

  /**
   * Add language related options to the command.
   *
   * @hook option vocabulary:create
   */
  public function hookVocabularyOptions(Command $command, AnnotationData $annotationData): void {
    $this->doAddOptions($command);
  }

  /**
   * Prompt for values for the language related options.
   *
   * @hook on-event node-type-set-options
   */
  public function hookNodeTypeSetOptions(InputInterface $input): void {
    $this->doSetOptions();
  }

  /**
   * Prompt for values for the language related options.
   *
   * @hook on-event vocabulary-set-options
   */
  public function hookVocabularySetOptions(InputInterface $input): void {
    $this->doSetOptions();
  }

  /**
   * Assign the values for the language related options to the config array.
   *
   * @hook on-event nodetype-create
   */
  public function hookNodeTypeCreate(array &$values): void {
    $this->doSetValues($values);
  }

  /**
   * Assign the values for the language related options to the config array.
   *
   * @hook on-event vocabulary-create
   */
  public function hookVocabularyCreate(array &$values): void {
    $this->doSetValues($values);
  }

  /**
   * Create content language settings config for an entity type.
   *
   * @hook post-command nodetype:create
   */
  public function hookPostNodeTypeCreate($result, CommandData $commandData): void {
    $this->doCreateContentLanguageSettings('node');
  }

  /**
   * Create content language settings config for an entity type.
   *
   * @hook post-command vocabulary:create
   */
  public function hookPostVocabularyCreate($result, CommandData $commandData): void {
    $this->doCreateContentLanguageSettings('taxonomy_vocabulary');
  }

  /**
   * Add language related options to a command.
   */
  protected function doAddOptions(Command $command): void {
    if (!$this->isInstalled()) {
      return;
    }

    $command->addOption(
      'default-language',
      '',
      InputOption::VALUE_OPTIONAL,
      'The default language of new entities.'
    );

    $command->addOption(
      'show-language-selector',
      '',
      InputOption::VALUE_OPTIONAL,
      'Whether to show the language selector on create and edit pages.'
    );
  }

  /**
   * Prompt for values for the language related options.
   */
  protected function doSetOptions(): void {
    if (!$this->isInstalled()) {
      return;
    }

    $this->ensureOption(
      'default-language',
      [$this, 'askLanguageDefault'],
      TRUE,
    );

    $this->ensureOption(
      'show-language-selector',
      [$this, 'askLanguageShowSelector'],
      TRUE
    );
  }

  /**
   * Assign the values for the language related options to the config array.
   */
  protected function doSetValues(array &$values): array {
    if (!$this->isInstalled()) {
      return $values;
    }

    $values['langcode'] = $this->input->getOption('default-language');
    $values['dependencies']['module'][] = 'language';

    return $values;
  }

  /**
   * Create content language settings config for an entity type.
   */
  protected function doCreateContentLanguageSettings(string $entityTypeId): void {
    if (!$this->isInstalled()) {
      return;
    }

    $bundle = $this->input->getOption('machine-name');
    $defaultLanguage = $this->input->getOption('default-language');
    $showLanguageSelector = (bool) $this->input->getOption('show-language-selector');

    $config = ContentLanguageSettings::loadByEntityTypeBundle($entityTypeId, $bundle);
    $config->setDefaultLangcode($defaultLanguage)
      ->setLanguageAlterable($showLanguageSelector)
      ->save();
  }

  /**
   * Prompt for the default value option.
   */
  protected function askLanguageDefault(): string {
    $options = [
      LanguageInterface::LANGCODE_SITE_DEFAULT => dt("Site's default language (@language)", ['@language' => \Drupal::languageManager()->getDefaultLanguage()->getName()]),
      'current_interface' => dt('Interface text language selected for page'),
      'authors_default' => dt("Author's preferred language"),
    ];

    $languages = $this->languageManager->getLanguages(LanguageInterface::STATE_ALL);

    foreach ($languages as $langcode => $language) {
      $options[$langcode] = $language->isLocked()
        ? dt('- @name -', ['@name' => $language->getName()])
        : $language->getName();
    }

    return $this->io()->choice('Default language', $options, 0);
  }

  /**
   * Prompt for values for the language related options.
   */
  protected function askLanguageShowSelector(): bool {
    return $this->io()->confirm('Show language selector on create and edit pages', FALSE);
  }

  /**
   * Check whether the language module is installed.
   */
  protected function isInstalled(): bool {
    return $this->moduleHandler->moduleExists('language');
  }

  /**
   * Prompt the user for the option if it's empty.
   */
  protected function ensureOption(string $name, callable $asker, bool $required): void {
    $value = $this->input->getOption($name);

    if ($value === NULL) {
      $value = $asker();
    }

    if ($required && $value === NULL) {
      throw new \InvalidArgumentException(dt('The %optionName option is required.', [
        '%optionName' => $name,
      ]));
    }

    $this->input->setOption($name, $value);
  }

}
