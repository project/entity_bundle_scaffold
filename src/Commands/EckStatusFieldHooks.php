<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\eck_status_field\Entity\PublishedEckEntityType;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;

/**
 * Drush hooks for integrating the eck_status_field module with other commands.
 */
class EckStatusFieldHooks extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a EckStatusFieldHooks object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Add eck_status_field related options to a command.
   *
   * @hook option eck:type:create
   */
  public function hookOption(Command $command): void {
    if (!$this->getStatusKey()) {
      return;
    }

    $command->addOption(
      'status',
      '',
      InputOption::VALUE_OPTIONAL,
      'Install the status base field.'
    );
  }

  /**
   * Prompt for values for the menu_ui related options.
   *
   * @hook interact eck:type:create
   */
  public function hookInteract(): void {
    if (!$this->getStatusKey()) {
      return;
    }

    $this->ensureOption('status', [$this, 'askStatusField'], FALSE);
  }

  /**
   * Assign the values for the language related options to the config array.
   *
   * @hook on-event eck-type-create
   */
  public function hookCreate(array &$values): void {
    $key = $this->getStatusKey();
    if ($key === NULL) {
      return;
    }

    $values[$key] = $this->input->getOption('status');

    if ($this->moduleHandler->moduleExists('eck_status_field')) {
      $values['dependencies']['module'][] = 'eck_status_field';
    }
  }

  /**
   * Check whether ECK entities are capable of having a status field.
   */
  protected function getStatusKey(): ?string {
    $entityType = $this->entityTypeManager
      ->getStorage('eck_entity_type')
      ->create();

    if (method_exists($entityType, 'hasStatusField')) {
      return 'status';
    }

    if (class_exists(PublishedEckEntityType::class) && $entityType instanceof PublishedEckEntityType) {
      return 'published';
    }

    return NULL;
  }

  /**
   * Ask whether the status base field should be added to the entity type.
   */
  protected function askStatusField(): bool {
    return $this->io()->confirm("Add the 'status' base field?");
  }

  /**
   * Prompt the user for the option if it's empty.
   */
  protected function ensureOption(string $name, callable $asker, bool $required): void {
    $value = $this->input->getOption($name);

    if ($value === NULL) {
      $value = $asker();
    }

    if ($required && $value === NULL) {
      throw new \InvalidArgumentException(dt('The %optionName option is required.', [
        '%optionName' => $name,
      ]));
    }

    $this->input->setOption($name, $value);
  }

}
