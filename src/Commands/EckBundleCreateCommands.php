<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Consolidation\AnnotatedCommand\AnnotationData;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareInterface;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\eck\Entity\EckEntityBundle;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Drush commands for creating ECK bundles.
 */
class EckBundleCreateCommands extends DrushCommands implements CustomEventAwareInterface {

  use BundleMachineNameAskTrait;
  use CustomEventAwareTrait;
  use EckEntityTypeTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an EckBundleCreateCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Create a new eck entity type.
   *
   * @param string|null $entityType
   *   The machine name of the entity type.
   * @param array $options
   *   The command options.
   *
   * @command eck:bundle:create
   * @aliases eck-bundle-create,ebc
   *
   * @option label
   *   The human-readable name of this entity bundle.
   *   This name must be unique.
   * @option machine-name
   *   A unique machine-readable name for this entity type bundle.
   *   It must only contain lowercase letters, numbers, and underscores.
   * @option description
   *   Describe this entity type bundle.
   *
   * @option show-machine-names
   *   Show machine names instead of labels in option lists.
   *
   * @usage drush eck:bundle:create
   *   Create an eck entity type by answering the prompts.
   *
   * @validate-module-enabled eck
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function create(?string $entityType = null, array $options = [
    'label' => InputOption::VALUE_REQUIRED,
    'machine-name' => InputOption::VALUE_REQUIRED,
    'description' => InputOption::VALUE_OPTIONAL,
    'show-machine-names' => InputOption::VALUE_OPTIONAL,
  ]): void {
    $definition = $this->entityTypeManager->getDefinition(sprintf('%s_type', $entityType));
    $storage = $this->entityTypeManager->getStorage(sprintf('%s_type', $entityType));

    $values = [
      'status' => TRUE,
      $definition->getKey('id') => $this->input()->getOption('machine-name'),
      $definition->getKey('label') => $this->input()->getOption('label'),
      'description' => $this->input()->getOption('description') ?? '',
      'entity_type' => $entityType,
    ];

    // Command files may customize $values as desired.
    $handlers = $this->getCustomEventHandlers('eck-bundle-create');
    foreach ($handlers as $handler) {
      $handler($values);
    }

    $bundle = $storage->create($values);
    $bundle->save();

    $this->entityTypeManager->clearCachedDefinitions();
    $this->logResult($bundle);
  }

  /**
   * Fill the command options by prompting the user.
   *
   * @hook interact eck:bundle:create
   */
  public function interact(InputInterface $input, OutputInterface $output, AnnotationData $annotationData): void {
    $this->input->setArgument(
      'entityType',
      $entityTypeId = $this->input->getArgument('entityType') ?? $this->askEckEntityType()
    );
    $this->validateEckEntityType($entityTypeId);

    $this->input->setOption(
      'label',
      $this->input->getOption('label') ?? $this->askLabel()
    );
    $this->input->setOption(
      'machine-name',
      $this->input->getOption('machine-name') ?? $this->askMachineName($entityTypeId)
    );
    $this->input->setOption(
      'description',
      $this->input->getOption('description') ?? $this->askDescription()
    );
  }

  /**
   * Ask for a human-readable label.
   */
  protected function askLabel(): string {
    return $this->io()->askRequired('Human-readable name');
  }

  /**
   * Ask for a description.
   */
  protected function askDescription(): ?string {
    return $this->io()->ask('Description');
  }

  /**
   * Log the command results.
   */
  private function logResult(EckEntityBundle $bundle): void {
    $this->logger()->success(
      sprintf("Successfully created %s bundle '%s'", $bundle->getEckEntityTypeMachineName(), $bundle->id())
    );

    $this->logger()->success(
      'Further customisation can be done at the following url:'
      . PHP_EOL
      . $bundle->toUrl()
        ->setAbsolute(TRUE)
        ->toString()
    );
  }

}
