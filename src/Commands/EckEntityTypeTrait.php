<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Trait providing common methods for dealing with ECK entity types.
 *
 * @property EntityTypeManagerInterface $entityTypeManager
 */
trait EckEntityTypeTrait {

  /**
   * Ask the user to select an ECK entity type.
   *
   * @return string|null
   *   The selected entity type ID.
   */
  protected function askEckEntityType(): ?string {
    $entityTypeDefinitions = array_filter(
      $this->entityTypeManager->getDefinitions(),
      function (EntityTypeInterface $entityType) {
        return $entityType->entityClassImplements(FieldableEntityInterface::class)
          && $entityType->getProvider() === 'eck';
      }
    );
    $choices = [];

    foreach ($entityTypeDefinitions as $entityTypeDefinition) {
      $choices[$entityTypeDefinition->id()] = $this->input->getOption('show-machine-names')
        ? $entityTypeDefinition->id()
        : $entityTypeDefinition->getLabel();
    }

    if (!$answer = $this->io()->choice('Entity type', $choices)) {
      throw new \InvalidArgumentException(t('The entityType argument is required.'));
    }

    return $answer;
  }

  /**
   * Validate whether an entity type exists and is an ECK entity type.
   */
  public function validateEckEntityType(string $entityTypeId): void {
    if (!$this->entityTypeManager->hasDefinition($entityTypeId)) {
      throw new \InvalidArgumentException(
        t("Entity type with id ':entityType' does not exist.", [':entityType' => $entityTypeId])
      );
    }

    $definition = $this->entityTypeManager->getDefinition($entityTypeId);
    if ($definition->getProvider() !== 'eck') {
      throw new \InvalidArgumentException(
        t("Entity type with id ':entityType' is not an ECK entity type.", [':entityType' => $entityTypeId])
      );
    }
  }

}
