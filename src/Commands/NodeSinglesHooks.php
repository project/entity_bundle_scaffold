<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;

/**
 * Drush hooks for integrating the node_singles module with other commands.
 */
class NodeSinglesHooks extends DrushCommands {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a NodeSinglesHooks object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Add node_singles related options to the command.
   *
   * @hook option nodetype:create
   */
  public function hookOption(Command $command): void {
    if (!$this->isInstalled()) {
      return;
    }

    $command->addOption(
      'is-single',
      '',
      InputOption::VALUE_OPTIONAL,
      'Is a content type with a single entity.'
    );
  }

  /**
   * Prompt for values for the language related options.
   *
   * @hook on-event node-type-set-options
   */
  public function hookSetOptions(): void {
    if (!$this->isInstalled()) {
      return;
    }

    $this->input->setOption(
      'is-single',
      $this->input->getOption('is-single') ?? $this->askIsSingle()
    );
  }

  /**
   * Assign the values for the language related options to the config array.
   *
   * @hook on-event nodetype-create
   */
  public function hookCreate(array &$values): void {
    if (!$this->isInstalled()) {
      return;
    }

    $values['third_party_settings']['node_singles']['is_single'] = (int) $this->input()->getOption('is-single');
    $values['dependencies']['module'][] = 'node_singles';
  }

  /**
   * Prompt whether the node type should be a single.
   */
  protected function askIsSingle(): bool {
    return $this->io()->confirm('Content type with a single entity?', FALSE);
  }

  /**
   * Check whether the node_singles module is installed.
   */
  protected function isInstalled(): bool {
    return $this->moduleHandler->moduleExists('node_singles');
  }

}
