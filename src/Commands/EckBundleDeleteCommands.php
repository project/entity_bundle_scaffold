<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Consolidation\AnnotatedCommand\Events\CustomEventAwareInterface;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareTrait;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\eck\Entity\EckEntityBundle;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\field\EntityTypeBundleAskTrait;
use Drush\Drupal\Commands\field\EntityTypeBundleValidationTrait;
use Symfony\Component\Console\Input\InputOption;

/**
 * Drush commands for deleting ECK bundles.
 */
class EckBundleDeleteCommands extends DrushCommands implements CustomEventAwareInterface {

  use CustomEventAwareTrait;
  use EntityTypeBundleAskTrait;
  use EntityTypeBundleValidationTrait;
  use EckEntityTypeTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs an EckBundleDeleteCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * Delete an eck entity type.
   *
   * @param string|null $entityType
   *   The machine name of the entity type.
   * @param string|null $bundle
   *   The machine name of the bundle.
   * @param array $options
   *   The command options.
   *
   * @command eck:bundle:delete
   * @aliases eck-bundle-delete,ebd
   *
   * @option show-machine-names
   *   Show machine names instead of labels in option lists.
   *
   * @option label
   *   The human-readable name of this entity bundle.
   *   This name must be unique.
   * @option machine-name
   *   A unique machine-readable name for this entity type bundle.
   *   It must only contain lowercase letters, numbers, and underscores.
   * @option description
   *   Describe this entity type bundle.
   *
   * @usage drush eck:bundle:delete
   *   Delete an eck entity type by answering the prompts.
   *
   * @validate-module-enabled eck
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function delete(?string $entityType = NULL, ?string $bundle = NULL, array $options = [
    'label' => InputOption::VALUE_REQUIRED,
    'machine-name' => InputOption::VALUE_REQUIRED,
    'description' => InputOption::VALUE_OPTIONAL,
    'show-machine-names' => InputOption::VALUE_OPTIONAL,
  ]): void {
    $this->input->setArgument('entityType', $entityType = $entityType ?? $this->askEckEntityType());
    $this->validateEckEntityType($entityType);

    $this->input->setArgument('bundle', $bundle = $bundle ?? $this->askBundle());
    $this->validateBundle($entityType, $bundle);

    $definition = $this->entityTypeManager->getDefinition(sprintf('%s_type', $entityType));
    $storage = $this->entityTypeManager->getStorage(sprintf('%s_type', $entityType));

    $bundles = $storage->loadByProperties([$definition->getKey('id') => $bundle]);
    $bundle = reset($bundles);

    // Command files may customize $values as desired.
    $handlers = $this->getCustomEventHandlers('eck-bundle-delete');
    foreach ($handlers as $handler) {
      $handler($bundle);
    }

    $storage->delete([$bundle]);

    $this->entityTypeManager->clearCachedDefinitions();
    $this->logResult($bundle);
  }

  /**
   * Log the command results.
   */
  private function logResult(EckEntityBundle $bundle): void {
    $this->logger()->success(
      sprintf("Successfully deleted %s bundle '%s'", $bundle->getEckEntityTypeMachineName(), $bundle->id())
    );
  }

}
