<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_bundle_scaffold\Service\Generator\EntityBundleClassGenerator;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\field\EntityTypeBundleAskTrait;
use Drush\Drupal\Commands\field\EntityTypeBundleValidationTrait;
use PhpParser\PrettyPrinter\Standard as PrettyPrinter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Drush commands for generating entity bundle classes.
 */
class EntityBundleClassCommands extends DrushCommands {

  use EntityTypeBundleAskTrait;
  use EntityTypeBundleValidationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity bundle class generator.
   *
   * @var \Drupal\entity_bundle_scaffold\Service\Generator\EntityBundleClassGenerator
   */
  protected $entityBundleClassGenerator;

  /**
   * The PHP code pretty printer.
   *
   * @var \PhpParser\PrettyPrinter\Standard
   */
  protected $prettyPrinter;

  /**
   * The filesystem service.
   *
   * @var \Symfony\Component\Filesystem\Filesystem
   */
  protected $fileSystem;

  /**
   * Constructs an EntityBundleClassCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\entity_bundle_scaffold\Service\Generator\EntityBundleClassGenerator $entityBundleClassGenerator
   *   The entity bundle class generator.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    ConfigFactoryInterface $configFactory,
    EntityBundleClassGenerator $entityBundleClassGenerator
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->configFactory = $configFactory;
    $this->entityBundleClassGenerator = $entityBundleClassGenerator;
    $this->prettyPrinter = new PrettyPrinter();
    $this->fileSystem = new Filesystem();
  }

  /**
   * Generate an entity bundle class.
   *
   * @param string|null $entityType
   *   The machine name of the entity type.
   * @param string|null $bundle
   *   The machine name of the bundle.
   * @param array $options
   *   The command options.
   *
   * @command entity:bundle-class-generate
   * @aliases entity-bundle-class-generate,ebcg
   *
   * @option module
   *   The module in which to generate the file
   *
   * @option show-machine-names
   *   Show machine names instead of labels in option lists.
   *
   * @usage drush entity-bundle-class-generate taxonomy_term tag
   *   Generate an entity bundle class.
   * @usage drush entity:bundle-class-generate
   *   Generate an entity bundle class and fill in the
   *   remaining information through prompts.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \ReflectionException
   */
  public function generate(?string $entityType = null, ?string $bundle = NULL, array $options = [
    'output-module' => InputOption::VALUE_REQUIRED,
    'show-machine-names' => InputOption::VALUE_OPTIONAL,
  ]): void {
    if (empty($options['output-module'])) {
      throw new \InvalidArgumentException('You must specify an output module through --output-module or through configuration.');
    }

    $this->input->setArgument('entityType', $entityType = $entityType ?? $this->askEntityType());
    $this->validateEntityType($entityType);

    $this->input->setArgument('bundle', $bundle = $bundle ?? $this->askBundle());
    $this->validateBundle($entityType, $bundle);

    $statements = [];
    $definition = $this->entityTypeManager->getDefinition($entityType);
    $existingClassName = $this->entityTypeManager->getStorage($entityType)->getEntityClass($bundle);
    $hasExisting = FALSE;

    if ($existingClassName && $existingClassName !== $definition->getOriginalClass()) {
      $hasExisting = TRUE;
      $destination = (new \ReflectionClass($existingClassName))->getFileName();

      if (file_exists($destination) && !$this->io()->confirm(sprintf('%s already exists. Append to existing class?', $existingClassName), FALSE)) {
        return;
      }

      $statements[] = $this->entityBundleClassGenerator->generateExisting($entityType, $bundle);
    }
    else {
      $destination = $this->entityBundleClassGenerator->buildEntityBundleClassPath($entityType, $bundle, $options['output-module']);
      $statements[] = $this->entityBundleClassGenerator->generateNew($entityType, $bundle, $options['output-module']);
    }

    $output = $this->prettyPrinter->prettyPrintFile($statements);
    $this->fileSystem->remove($destination);
    $this->fileSystem->appendToFile($destination, $output);

    $this->logger()->success(
      sprintf('Successfully %s entity bundle class.', $hasExisting ? 'updated' : 'created')
    );

    if (!$this->entityBundleClassGenerator->areModelsAnnotationBased()) {
      $this->logger()->warning("Don't forget to register your bundle class through a <href=https://www.drupal.org/node/3191609>hook_entity_bundle_info_alter implementation</>, or by using the <href=https://www.drupal.org/project/entity_model>Entity Model module</>.");
    }

    // Clear cached bundle classes.
    $this->entityBundleClassGenerator->clearEntityBundleCaches();
  }

  /**
   * Set the default value for the output-module option.
   *
   * @hook init entity:bundle-class-generate
   */
  public function init(): void {
    $module = $this->input->getOption('output-module');

    if (!$module) {
      $default = $this->configFactory
        ->get('entity_bundle_scaffold.settings')
        ->get('generators.bundle_class.output_module');

      $this->input->setOption('output-module', $default);
    }
  }

}
