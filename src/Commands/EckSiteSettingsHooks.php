<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;

/**
 * Drush hooks for integrating the eck_site_settings module with other commands.
 */
class EckSiteSettingsHooks extends DrushCommands {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a EckSiteSettingsHooks object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Add options to the command.
   *
   * @hook option eck:type:create
   */
  public function hookOption(Command $command): void {
    if (!$this->isInstalled()) {
      return;
    }

    $command->addOption(
      'is-setting',
      '',
      InputOption::VALUE_OPTIONAL,
      'Use this entity type for site settings.'
    );
  }

  /**
   * Prompt for values.
   *
   * @hook interact eck:type:create
   */
  public function hookSetOptions(): void {
    if (!$this->isInstalled()) {
      return;
    }

    $this->input->setOption(
      'is-setting',
      $this->input->getOption('is-setting') ?? $this->askIsSetting()
    );
  }

  /**
   * Assign the values for the options to the config array.
   *
   * @hook on-event eck-type-create
   */
  public function hookCreate(array &$values): void {
    if (!$this->isInstalled()) {
      return;
    }

    $values['third_party_settings']['eck_site_settings']['enabled'] = (int) $this->input()->getOption('is-setting');
    $values['dependencies']['module'][] = 'eck_site_settings';
  }

  /**
   * Prompt whether the ECK entity type should be a site setting.
   */
  protected function askIsSetting(): bool {
    return $this->io()->confirm('Use this entity type for site settings?', FALSE);
  }

  /**
   * Check whether the eck_site_settings module is installed.
   */
  protected function isInstalled(): bool {
    return $this->moduleHandler->moduleExists('eck_site_settings');
  }

}
