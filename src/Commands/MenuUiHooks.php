<?php

namespace Drupal\entity_bundle_scaffold\Commands;

use Consolidation\AnnotatedCommand\AnnotationData;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Menu\MenuParentFormSelectorInterface;
use Drupal\system\Entity\Menu;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Drush hooks for integrating the menu_ui module with other commands.
 */
class MenuUiHooks extends DrushCommands {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The menu parent form selector.
   *
   * @var \Drupal\Core\Menu\MenuParentFormSelectorInterface
   */
  protected $menuParentFormSelector;

  /**
   * Constructs a MenuUiHooks object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Menu\MenuParentFormSelectorInterface $menuParentFormSelector
   *   The menu parent form selector.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    MenuParentFormSelectorInterface $menuParentFormSelector
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->menuParentFormSelector = $menuParentFormSelector;
  }

  /**
   * Add menu_ui related options to a command.
   *
   * @hook option nodetype:create
   */
  public function hookOption(Command $command, AnnotationData $annotationData): void {
    if (!$this->isInstalled()) {
      return;
    }

    $command->addOption(
      'menus-available',
      '',
      InputOption::VALUE_OPTIONAL,
      'The menus available to place links in for this content type.'
    );

    $command->addOption(
      'menu-default-parent',
      '',
      InputOption::VALUE_OPTIONAL,
      'The menu item to be the default parent for a new link in the content authoring form.'
    );
  }

  /**
   * Prompt for values for the menu_ui related options.
   *
   * @hook on-event node-type-set-options
   */
  public function hookSetOptions(InputInterface $input): void {
    if (!$this->isInstalled()) {
      return;
    }

    $this->ensureOption('menus-available', [$this, 'askMenus'], TRUE);
    $menus = $this->input->getOption('menus-available');
    if (is_string($menus)) {
      $menus = explode(',', $menus);
      $this->input->setOption('menus-available', $menus);
    }

    if ($menus === []) {
      return;
    }

    $this->ensureOption('menu-default-parent', [$this, 'askDefaultParent'], TRUE);
  }

  /**
   * Assign the values for the language related options to the config array.
   *
   * @hook on-event nodetype-create
   */
  public function hookCreate(array &$values): void {
    if (!$this->isInstalled()) {
      return;
    }

    $values['third_party_settings']['menu_ui']['available_menus'] = $this->input->getOption('menus-available');
    $values['third_party_settings']['menu_ui']['parent'] = $this->input->getOption('menu-default-parent') ?? '';
    $values['dependencies']['module'][] = 'menu_ui';
  }

  /**
   * Prompt for the menus option.
   */
  protected function askMenus(): array {
    $menus = Menu::loadMultiple();
    $choices = ['_none' => '- None -'];

    foreach ($menus as $name => $menu) {
      $label = $this->input->getOption('show-machine-names') ? $name : $menu->label();
      $choices[$name] = $label;
    }

    $question = (new ChoiceQuestion('Available menus', $choices, '_none'))
      ->setMultiselect(TRUE);

    return array_filter(
          $this->io()->askQuestion($question) ?: [],
          function (string $value) {
              return $value !== '_none';
          }
      );
  }

  /**
   * Prompt for the default parent option.
   */
  protected function askDefaultParent(): string {
    $menus = $this->input->getOption('menus-available');
    $menus = array_intersect_key(Menu::loadMultiple(), array_flip($menus));

    $options = array_map(function (Menu $menu) {
      return $menu->label();
    }, $menus);
    $options = $this->menuParentFormSelector->getParentSelectOptions('', $options);

    return $this->io()->choice('Default parent item', $options, 0);
  }

  /**
   * Check whether the menu_ui module is installed.
   */
  protected function isInstalled(): bool {
    return $this->moduleHandler->moduleExists('menu_ui');
  }

  /**
   * Prompt the user for the option if it's empty.
   */
  protected function ensureOption(string $name, callable $asker, bool $required): void {
    $value = $this->input->getOption($name);

    if ($value === NULL) {
      $value = $asker();
    }

    if ($required && $value === NULL) {
      throw new \InvalidArgumentException(dt('The %optionName option is required.', [
        '%optionName' => $name,
      ]));
    }

    $this->input->setOption($name, $value);
  }

}
