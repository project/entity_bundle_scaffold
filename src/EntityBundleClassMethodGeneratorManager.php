<?php

namespace Drupal\entity_bundle_scaffold;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_bundle_scaffold\Annotation\EntityBundleClassMethodGenerator;

/**
 * Plugin manager for entity bundle class method generator plugins.
 */
class EntityBundleClassMethodGeneratorManager extends DefaultPluginManager {

  /**
   * Constructs an EntityBundleClassMethodGeneratorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct(
      'Plugin/EntityBundleClassMethodGenerator',
      $namespaces,
      $moduleHandler,
      EntityBundleClassMethodGeneratorInterface::class,
      EntityBundleClassMethodGenerator::class
    );
    $this->alterInfo('entity_bundle_class_method_generator');
    $this->setCacheBackend($cacheBackend, 'entity_bundle_class_method_generator_plugins');
  }

}
